using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementOfMisil : MonoBehaviour
{

    private float _speed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        transform.position += new Vector3(0, _speed * Time.deltaTime, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, _speed * Time.deltaTime, 0);
    }
}
