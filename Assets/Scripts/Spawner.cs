using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] objectsToSpawn;

    public float timeToSpawn = 1f;
    private float currentTimeToSpawn;
    private Vector3 _initposition;


    // Start is called before the first frame update
    private void Start()
    {
        currentTimeToSpawn = timeToSpawn;
    }

    // Update is called once per frame
    private void Update()
    {
        _initposition = new Vector3((Random.Range(-10.0f, 10.0f)), transform.position.y, transform.position.z);

        CountTimeForNexTInstantiate();
    }



    private void CountTimeForNexTInstantiate()
    {
        if (currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;
        }
        else
        {
            SpawnObject(0);
            currentTimeToSpawn = timeToSpawn;
        }

    }

    private void SpawnObject(int numOrderObjectToSpawn)
    {
        Instantiate(objectsToSpawn[numOrderObjectToSpawn], _initposition, transform.rotation);
    }
}
