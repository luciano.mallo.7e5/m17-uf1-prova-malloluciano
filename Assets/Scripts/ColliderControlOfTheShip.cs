using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColliderControlOfTheShip : MonoBehaviour
{
    // private PolygonCollider2D _shipCollider;

    private void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.collider.tag == "Enemie" || collision.collider.tag == "EnemieMissil")
        {
            SceneManager.LoadScene("Start");
        }
    }



}
