using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    private static int _score = 0;

    public static GameManager Instance
    {
        get
        {

            if (_instance is null)
            {
                Debug.LogError("GameManager MAnager is null");
            }
            return _instance;
        }
    }


    public static int GetScore()
    {
        return _score;
    }

    public static void AddToScore(int toaddscore)
    {
        _score += toaddscore;
    }

    private void Awake()
    {
        _instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
