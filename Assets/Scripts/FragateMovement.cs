using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateMovement : MonoBehaviour
{
    public GameObject misilPrefab;
    public float timeToSpawn = 1f;
    private float currentTimeToSpawn;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Missil")
        {
            GameManager.AddToScore(5);
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.collider.tag == "Land")
        {
            Destroy(collision.gameObject);
        }

    }

    private void Fire()
    {
        if (currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;
        }
        else
        {
            Transform gunGO = GameObject.Find("Alien - Fragate").transform;
            Instantiate(misilPrefab, gunGO.position, gunGO.rotation);
            currentTimeToSpawn = timeToSpawn;
        }




    }
}
