using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float _speed = 2f;
    public GameObject misilPrefab;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        MoveCharecter();
        Fire();
    }

    private void MoveCharecter()
    {
        transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * _speed, Input.GetAxis("Vertical") * Time.deltaTime * _speed, 0);
    }
    private void Fire()
    {
        if (Input.GetKeyDown("space"))
        {
            Transform gunGO = GameObject.Find("2DFighterMinigun").transform;
            Instantiate(misilPrefab, gunGO.position, gunGO.rotation);
        }
    }
}
