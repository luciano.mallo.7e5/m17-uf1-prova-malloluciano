using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieController : MonoBehaviour
{

    private float _maxDistance = 4f;
    private float _minDistance = -4f;
    private float _moveDirection = -1f;
    private float _speed = 5f;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Missil")
        {
            GameManager.AddToScore(5);
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.collider.tag == "Land")
        {
            Destroy(collision.gameObject);
        }

    }
}
